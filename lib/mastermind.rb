class Code
  attr_reader :pegs
  DEFAULT = %w(R G B Y O P)
  PEGS = { 'R'=> :Red, 'G'=> :Green, 'B'=>:Black, 'Y'=>:Yellow, 'O'=>:Orange, 'P'=>:Purple }


  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(input)
    pegs = []
    input.split('').map do |peg|
      if PEGS.keys.include?(peg.upcase)
        pegs << PEGS[peg.upcase]
      else
        raise 'parse error'
      end
    end
    Code.new(pegs) if pegs.length == 4
  end

  def self.random
    pegs = []
    4.times do
      pegs << PEGS[DEFAULT[rand(5)]]
    end
    Code.new(pegs)
  end

  def ==(other)
    other.class == self.class && exact_matches(other) == 4
  end

  def exact_matches(other_code)
    return 0 if other_code.pegs.empty?
    exact_count = 0
    4.times do |n|
      exact_count += 1 if @pegs[n].upcase == other_code.pegs[n].upcase
    end
    exact_count
  end

  def near_matches(other_code)
    near = []
    pegs1 = @pegs
    pegs2 = other_code.pegs
    pegs2.each_with_index do |peg, idx|
      near << peg if pegs1[idx] != peg && pegs1.include?(peg)
    end
    a = near.uniq.map do |peg|
      [pegs2.count(peg), near.count(peg)].min
    end.compact.reduce(0, :+)
    b = near.uniq.map do |peg|
      [pegs1.count(peg), near.count(peg)].min
    end.compact.reduce(0, :+)
    [a, b].min
  end

  def [](i)
    pegs[i]
  end
end


class Game
  attr_reader :secret_code
  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "what is your guess?"
    input = gets.chomp
    Code.parse(input)
  end

  def display_matches(code)
    puts "exact match" + @secret_code.exact_matches(code).to_s
    puts "near match" + @secret_code.near_matches(code).to_s

  end
end
